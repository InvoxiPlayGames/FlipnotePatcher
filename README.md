FlipnotePatcher
==========

(Based on [WfcPatcher](https://github.com/AdmiralCurtiss/WfcPatcher) by [AdmiralCurtiss](https://github.com/AdmiralCurtiss))

Patches Flipnote Studio ROMs to patch out HTTPS connections and allow them to connect to a Flipnote Hatena replacement server, such as [IPGFlip](https://www.ipgflip.xyz).

Just drag and drop your dumped Flipnote Studio ROM onto the executable to patch them.

The following command line options are supported:

    -d, --domain ugomemo.example.com            Point the Flipnote Hatena URLs to a different domain instead. (Default: flipnte.ipgflip.xyz)
    -n, --nas nas.example.com            Point the Nintendo WFC URLs to a different domain instead. (Default: nas.wiimmfi.de)
